#!/bin/sh
# set -e

rm -f *.js
rm -f *.css
rm -f *.map
grep -v '^#' assets.lst | wget -nv -i- -c
